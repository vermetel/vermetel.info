(ql:quickload :cl-strings)
(ql:quickload :hunchentoot)
(ql:quickload :ppath)

;;; Configuratie.
;; Het pad naar de map waar statische bestanden te vinden zijn.
(defvar *poort* 8037) ; De poort waarop de server luistert naar verbindingen.
(defvar *statisch-pad* (ppath:abspath "statisch")) ; Default: de map "statisch" in de huidige working dir.
;; Geef een stukje HTML dat de gebruiker in staat stelt contact op te nemen.
(defun link-contact (&rest inhoud)
  (if (not inhoud) (setf inhoud '("Neem contact op")))
  `(a :href "/over#contactgegevens" ,@inhoud)) ; pas dit aan naar bijvoorbeeld de "over ons"-pagina, of maak het een "mailto:"

(defvar *server* nil)
;; Start server als die er nog niet is.
;; Merk op dat we dit doen voordat de routes gedefineerd zijn,
;; maar dat is niet zo'n probleem omdat we dynamisch routes kunnen inladen.
(defun start-server ()
  (when (not *server*)
    (setq *server* (make-instance 'hunchentoot:easy-acceptor :port *poort*))
    (hunchentoot:start *server*)))
(start-server)

;;; Het routesysteem.
;; De routes die we tot nu toe kennen.
(defvar *routes* nil)
;; Stop een route met gegeven afhandelaar tussen de bekende routes.
;; De eerst toegevoegde route wordt als laatst geprobeerd,
;; dus je kan fout 404 geven door die onder '(pad) te zetten.
(defun nieuwe-route (naam route)
  (push (cons naam route) *routes*))

;; Definieer een route en een functie die die route afhandelt.
;; De route is een lijst van strings en symbolen,
;; deze worden beschouwd als de verschillende onderdelen van het pad.
;; Zo staat '("") (dus niet '() !) voor de hoofdpagina en '(\"v\" int) voor een pad van de vorm /v/<int>.
;; Symbolen staan dus voor variabele stukjes pad:
;; 'int : een geheel getal, bestaande uit de cijfers 0-9.
;; 'naam : een arbitraire streng die tussen /'en staat.
;; 'pad : een arbitraire streng die niet noodzakelijk tussen /'en staat. Moet als laatste in de lijst staan.
;; De naam, lambda-list en body definiëren samen een functie (die ook afzonderlijk aangeroepen kan worden).
;; Variabele stukjes pad worden in de gegeven volgorde als argumenten doorgegeven aan de afhandelaar.
;; Bijvoorbeeld:
;;   > (def-route hoofdpagina ("") () ("Hallo, Wereld!"))
(defmacro def-route (naam route lambda-list &body body)
  `(progn
     (nieuwe-route (quote ,naam) (quote ,route))
     (defun ,naam ,lambda-list ,@body)))

;; Probeert de route en de stukjes URL op elkaar te passen.
;; Hier moeten stringdelen overeenkomen met stringstukjes,
;; en symbooldelen correct werken.
(defun vergelijk-route-met-stukjes (route stukjes)
  (cond
    ((and (not route) (not stukjes)) (values t '()))
    ((or (not route) (not stukjes)) (values nil nil))
    (t (multiple-value-bind (rest-succes rest-args) (vergelijk-route-met-stukjes (rest route) (rest stukjes))
         (case (first route)
           ((pad) (values t (cons stukjes nil))) ; bij een pad maakt het niet uit wat er met de rest gebeurt.
           ((int) (handler-case (values rest-succes (cons (parse-integer (first stukjes)) rest-args))
                    (parse-error () (values nil nil))))
           ((str) (values rest-succes (cons (first stukjes) rest-args)))
           (otherwise (if (equal (first route) (first stukjes))
                          (values rest-succes rest-args)
                          (values nil nil))))))))

;; Hunchentoot-dispatcher voor het werken met de 'hunchentoot:easy-acceptor.
;; Returnt de afhandelaar voor een route indien bestaand,
;; of anders nil.
;; Leest het opgevraagde pad uit van de gegeven request (standaard *request*).
(defun route-dispatcher (&optional (request hunchentoot:*request*))
  (let* ((volledige-uri (hunchentoot:request-uri* request))
         (relatieve-uri (subseq volledige-uri 1)) ; hak de begin-#\/ ervanaf.
         (stukjes (cl-strings:split relatieve-uri "/")))
    (loop named over-routes
          for routepaar in *routes*
          do (destructuring-bind (naam . route) routepaar
               (multiple-value-bind (succes args) (vergelijk-route-met-stukjes route stukjes)
                 (if succes
                     (return-from over-routes (lambda () (apply naam args)))))))))

;; Omgekeerde van route-dispatcher: geef een pad horend bij de gegeven afhandelaar.
(defun pad-naar (afhandelaar)
  (cdr (assoc afhandelaar *routes*)))

;; Vul de argumenten van een pad in, het pad omzettend in een lijst strengen.
(defun vul-pad-in (pad args)
  (cond
    ((not pad) nil)
    ((equal (first pad) 'pad) (first args))
    ((equal (first pad) 'str) (cons (first args) (vul-pad-in (rest pad) (rest args))))
    ((equal (first pad) 'int) (cons (format nil "~a" (first args)) (vul-pad-in (rest pad) (rest args))))
    ((stringp (first pad)) (cons (first pad) (vul-pad-in (rest pad) args)))
    (t (error "Onbekend padformaat ~a" pad))))

;; Zet een pad om in een URL, waarbij we de argumenten juist invullen.
;; De URL heeft geen / aan het begin.
(defun pad->url (pad args)
  (format nil "~{~a~^/~}" (vul-pad-in pad args))) ; plak met /'en aan elkaar.

;; Zet afhandelaar met args om in een URL.
;; De URL heeft wel een / aan het begin.
(defun url-naar (afhandelaar &rest args)
  (concatenate 'string "/" (pad->url (pad-naar afhandelaar) args)))

(push #'route-dispatcher hunchentoot:*dispatch-table*)

;; Geef een bijnaam voor een route:
;; deze stuurt de gebruiker door naar de gegeven ``echte'' pagina,
;; met dezelfde padargumenten ingevuld.
;; Indien de vervangende-args niet leeg zijn, worden die gebruikt als argumenten,
;; in plaats van die gegeven aan de bijnaamroute.
;; Ze worden uitgevoerd vlak voordat url-naar wordt aangeroepen.
(defmacro bijnaam-route (pad naam &optional (vervangende-args nil))
  `(nieuwe-route (lambda (&rest args)
                   (hunchentoot:redirect (apply 'url-naar (quote ,naam) ,(if vervangende-args `(list ,@vervangende-args) 'args))))
                 (quote ,pad)))

;;; Maak een HTML-DSL.
;;; We geven HTML weer als een s-expressie,
;;; bijvoorbeeld:
;;; (render-html `(html (head (title "Ik ben een website!") (link :rel "stylesheet" :type "text/css" :href ,(url-naar 'statisch-bestand '("basis.css")))) (body (p :class "pagina-inhoud" "Ik ben een website, is dat niet geweldig?"))))
;;; Strings worden niet ge-HTML-escaped.
(define-condition html-ongeldig (error)
  ((want :initarg :want :reader want))
  (:report (lambda (foutmelding stream)
             (format stream "Deze expressie is niet te ver-HTML'en. Specifiekere reden: ~a" (want foutmelding)))))

;; Vertaal HTML in lijstvorm naar een string.
(defun render-html (expressie)
  (cond
    ((stringp expressie) expressie)
    ((symbolp expressie) (error 'html-ongeldig :want "tags mogen niet los voorkomen: ze moeten in een lijst staan"))
    ((listp expressie) (render-html-tag (symbol-name (first expressie)) "" "" (rest expressie)))))

;; Vertaal een HTML-tag met gegeven attributen en inhoud (als string) naar string.
;; Ongeparst kan een lijst attributen en inhoud bevatten:
;; '(:attr "waarde" ...) wordt <tag attr=\"waarde\" ...>...</tag>,
;; en '("waarde1" "waarde2") wordt <tag>waarde1 waarde2</tag>.
(defun render-html-tag (tag attrs inhoud ongeparst)
  (cond
    ((not ongeparst) (format nil "<~a~a>~a</~a>" tag attrs inhoud tag)) ; want args beginnen met een spatie
    ((keywordp (first ongeparst)) (let ((attr (symbol-name (first ongeparst)))
                                        (waarde (second ongeparst)))
                                    (render-html-tag tag
                                                     (format nil "~a ~a=~s" attrs attr waarde) ; TEDOEN: maak dit minder breekbaar!
                                                     inhoud
                                                     (cddr ongeparst))))
    (t (render-html-tag tag
                        attrs
                        (format nil "~a~a" inhoud (render-html (first ongeparst)))
                        (rest ongeparst)))))

;; Geef een menuutje voor onderaan de pagina.
(defun bouw-menuutje (&key laatst-bijgewerkt citaat)
  `(div :class "menuutje"
        ,@(if laatst-bijgewerkt (list `(p ,(concatenate 'string "Deze pagina is laatst bijgewerkt op " laatst-bijgewerkt "."))))
        ,@(if citaat (list `(blockquote (p ,citaat))))
        (nav :role "navigation"
             (ul
              (li (a :href ,(url-naar 'voorblad) (img :class "inline-logo" :src ,(url-naar 'statisch-bestand '("vermetel.svg")) :alt "vermetel")))
              (li (a :href ,(url-naar 'over-vermetel) "over vermetel"))
              (li (a :href ,(url-naar 'nieuwigheden) "nieuwigheden"))))))

;; Genereer een standaardpagina, voor gebruik in render-html.
;; De titel is een string met de titel van de pagina.
;; De inhoud is een lijst s-expressies met html.
;; Het menuutje komt na de inhoud, en is in principe / per default de uitkomst van 'bouw-menuutje.
(defun standaardpagina (titel inhoud &key (menuutje nil))
  `(html
    (head
     (title ,titel)
     (link :rel "stylesheet" :type "text/css" :href ,(url-naar 'statisch-bestand '("basis.css"))))
    (body
     (div :class "pagina-inhoud" (list ,@inhoud))
     ,@(if menuutje (list menuutje) (list (bouw-menuutje))))))

;; Geef een citaat weer.
(defun citeer (&key (titel "Naamloos werk") (auteur "Anoniem") (datum nil) (url nil) (geraadpleegd nil))
  (let* ((titelstukje (if datum
                          (format nil "~a, '~a', ~a" auteur titel datum)
                          (format nil "~a, '~a'" auteur titel)))
         (citeertekst (if geraadpleegd
                          (format nil "[~a (geraadpleegd ~a)]" titelstukje geraadpleegd)
                          (format nil "[~a]" titelstukje))))
    `(span :class "citaat"
           ,(if url
                `(a :href ,url ,citeertekst)
                citeertekst))))

;; Definieer een "saaie pagina" die niet veel verandert.
;; Heeft een titel en inhoud,
;; die in de standaardpagina worden neergezet.
;; Merk op dat de inhoud uitgevoerd wordt.
;; De route mag geen variabele stukjes bevatten.
(defmacro def-saai (naam route &rest pagina-args)
  `(def-route ,naam ,route ()
     (render-html (standaardpagina ,@pagina-args))))

;;; Klaar met raamwerkopzetten, nu over naar inhoud in elkaar programmeren.

;;; Definieer de beschikbare routes.
;; Foutmelding 404 accepteert elk pad en wordt als eerste gedefinieerd,
;; want die wordt als laatste met het gevraagde pad vergeleken.
(def-route fout-niet-gevonden (pad) (gevraagde-pad)
  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
  (render-html (standaardpagina
                "404: Niet gevonden"
                (list (format nil "De pagina `~a' kan niet gevonden worden." gevraagde-pad)))))

;; Statische bestanden zijn te vinden in ... tromgeroffel ... "/statisch"!
(def-route statisch-bestand ("statisch" pad) (pad)
  (hunchentoot:handle-static-file (ppath:join *statisch-pad* (pad->url pad nil))))

;; We geven een vermetele uitspraak door een element te geven wat boven
;; p of niet p zit in de Rieger-Nishimuratralie.
(defun geef-vermetele-uitspraak (i)
  (incf i 5) ; p of niet p heeft index 5, begin daar dus
  (let ((psi "(p ∨ ¬ p)") (phi "¬¬p")) ; voor de leesbaarheid is de eerste stap op level 2
    (dotimes (j (floor (- (/ i 2) 2)))
      (setf phi (prog1
                    (format nil "(~a → ~a)" phi psi)
                  (setf psi (format nil "(~a ∨ ~a)" psi phi)))))
    (if (zerop (mod i 2))
        phi
        psi)))

;; Genereer vermetele uitspraken met gegeven index.
(def-route vermetel ("v" int) (index)
  (render-html (standaardpagina
                "vermetel.info"
                (list
                 `(p :class "uitspraak"
                     (a :href ,(url-naar 'vermetel (+ index 1))
                        ,(geef-vermetele-uitspraak index)))))))

;; Voorblad: geeft de eerste vermetele uitspraak.
(def-route voorblad ("") ()
  (vermetel 0))

(def-saai keuzeaxioma ("keuze")
  "het Keuzeaxioma"
  (list
   '(h1 "Het Keuzeaxioma")
   `(div :class "lead"
         (blockquote
          (p "The Axiom of Choice is obviously true; the well-ordering principle is obviously false; and who can tell about Zorn’s lemma? — Jerry Bona")
          ,(citeer
            :titel "The Axiom of Choice. In: Handbook of Logic and Proof Techniques for Computer Science."
            :auteur "Steven G. Krantz"
            :datum "2002"
            :url "https://link.springer.com/chapter/10.1007%2F978-1-4612-0115-1_9"))
         (p "Deze wijsheid wordt door klassieke wiskundigen gezien als grap: hoewel op het eerste gezicht de drie principes heel verschillend in werking en waarheid lijken, zijn voor hen het Keuzeaxioma, het Welordeningsprincipe en Zorns lemma precies even waar. Toch hoeft er helemaal geen tegenspraak tussen stelling en intuïtie te zitten."))
   '(p "Laten we precies maken waar we het over hebben en er de naam ``Keuzeparadox'' opplakken: er is een schijnbare tegenspraak tussen enerzijds de intuïtieve blik op elk van de drie principes, en anderzijds de implicaties die zouden moeeten aantonen dat de drie principes inwisselbaar zijn. Uit deze paradox kunnen we grofweg drie paden volgen: weerspreek de beschrijving van intuïtie in de Keuzeparadox, weerspreek de inwisselbaarheid tussen de drie principes, of weerspreek dat intuïtie en waarheid iets met elkaar hoeven te maken.")
   '(p "Wat betreft het weerspreken van intuïtie: dit beschouw ik als een volslagen oninteressante aanpak. Wat jij persoonlijk vindt van elk van de principes is iets waar alleen jij en eventuele Goden zeker van kunnen zijn, en net zo voor mijn mening op het eerste gezicht van elk van de principes. Er is geen garantie dat we elkaar ooit overtuigen op deze manier, dus waarom beschouwen we niet liever een van de andere twee uitwegen?")
   '(p "Weerspreken dat er verbinding tussen intuïtie en wiskundige zekerheid zou moeten zijn is een aantrekkelijkere keuze. Bovendien is dit een standpunt dat aan de lopende band ingenomen wordt tijdens het opvoeden van een nieuwe wiskundige. Het gemene van dit standpunt is echter dat het veel goed doet in het begin, maar wanneer tot het bittere eind toegepast leidt tot grote problemen. Iemand die echt gelooft in het ontkoppelen van intuïtie bij het doen van wiskunde en daadwerkelijk leeft hiernaar, is iemand die opgeeft om nuttig werk te verrichten. Een puur formele blik naar wiskunde strandt in betekenisloze symboolmanipulatie. Symboolmanipulatie kan heel bevredigend zijn voor de manipulator, maar verhoudt zich tot echte wiskunde als het lezen van een woordenboek tot het lezen van een roman. (Dit punt is verwant aan het volgende idee, wat ik nog eens wil uitwerken: de onredelijke effectiviteit van wiskunde is zo omdat de Wiskunde precies is wat universeel is aan denken, dus het is onmogelijk je iets te realiseren waarin wiskunde niet effectief is.)")
   '(p "Omdat beide alternatieven onaantrekkelijk blijken, zullen we toch echt moeten toegeven dat de intuïtieve blik informatie kan leveren over de waarheid van een gegeven principe. Oftewel: zijn de implicaties wel echt zo onontkomelijk als beweerd in de inleiding?")
   '(p "Ten eerste spreken het Keuzeaxioma, het Welordeningsprincipe en Zorns lemma van verschillende werelden. Het Keuzeaxioma leeft in een wereld van verzamelingen en functies, het Welordeningsprincipe in een wereld van deelverzamelingen en een ordeningsrelatie, en Zorns lemma in een wereld van een ordeningsrelatie, ketens, bovengrenzen en maximale elementen. Om te kunnen beginnen te spreken over een zinnige implicatie, moeten we eerst de werelden samenbrengen. Met andere woorden, we moeten een gemeenschappelijke taal kiezen. (Dit hoeft niet noodzakelijk een formele taal van predicatenlogica te zijn, maar dit kan enige houvast bieden mocht je dit hele zaakje niet vertrouwen.) De taal van verzamelingenleer kan alle andere nodige begrippen uitdrukken, dus dat is een voor de hand liggende keuze om een wereld te bouwen die alledrie de principes kan bevatten.")
   '(p "Vraag je een logicus die je op straat tegenkomt welke verzamelingen een wiskundige nou zoal tegenkomt, zal deze antwoorden dat ze doorgaans in ZFC leven: de verzamelingenleer van Zermelo en Fraenkel, samen met het Keuzeaxioma. Het zal hopelijk niet al te veel verbazing wekken dat in deze verzamelingenleer het Keuzeaxioma waar is, en zoals beloofd vallen het Welordeningsprincipe en Zorns lemma allebei ook te bewijzen binnen ZFC. Praten over de waarheid van het Keuzeaxioma als we dat axioma al aan hebben genomen, is natuurlijk je reinste valsspelerij, maar dat geeft niet. We kunnen ook kijken naar ZF zonder het Keuzeaxioma, en dan zijn we in staat om implicaties tussen elk van de drie principes vinden. Dus ze zijn alledrie even waar. Toch?")
   '(p "Wat de logici op de straat mogen beweren, zijn er heel weinig wiskundigen die echt denken over ZF, al dan niet met C, als iets anders dan een systeem waarin je toevallig veel nuttige dingen kunt doen. Als je kijkt naar wat wiskundigen echt doen, dan heeft dat vrij weinig te maken met deze specifieke axiomatisering van verzamelingenleer. Een snel proefje: ben je het meteen eens dat ``exp ⊆ <'' een welgevormde bewering is die hetzelfde betekent als ``de exponentiële functie is strikt stijgend''? Zo niet, dan moet je concluderen dat ZFC slechts de afspiegelingen bevat van de wiskunde die je echt doet.")))

(bijnaam-route ("licentie") statisch-bestand ('("agpl-3.0.txt")))

(def-saai nieuwigheden ("nieuw")
  "Nieuwigheden"
  (list
   '(h1 "Vermetele nieuwigheden")
   '(div :class "nieuwigheid"
    '(h2 "Vallende ster gespot vanuit Utrecht")
    '(p "In de vroege uurtjes van 2019-06-30 (dus de nacht van 2019-06-29 op 2019-06-30; tussen 0:00 en 1:00 's nachts vermoed ik), heb ik in de wijk Lunetten van Utrecht een opvallend felle meteoor gezien. Deze bewoog van het zuidoosten naar het oosten, ongeveer van ζ Aquilae naar ε Cygni. Het licht was erg fel, vergelijkbaar met een straatlantaarn op een paar honderd meter afstand, en had een blauwgroene kleur. Het liet een paar seconden een geel spoor van kleinere lichtjes achter, en verdween daarna. Het was een redelijk luidruchtige omgeving (op een feestje met achtergrondmuziek); ik kon geen knal horen. Helaas had ik geen fototoestel mee om het vast te leggen.")))
  :menuutje (bouw-menuutje
             :laatst-bijgewerkt "2019-07-01"))

;; Leg uit wat "vermetel" eigenlijk inhoudt.
(def-saai over-vermetel ("over")
  "Waar gaat het eigenlijk over?"
  (list
   '(p "Je vraagt je misschien af wie wij zijn. Nou, dat gaan we dan nu bekendmaken.")
   '(h1 "vermetel")
   '(p "In de woorden van de notoire Wim Veldman:")
   '(blockquote (p "Vermetel is when you go to slay the dragon, and you take your sword. You do not know if your sword is good enough, but you go anyway!"))
   `(p "In iets prozaïscher termen betekent het woord ``vermetel'' iets als ``heel erg dapper, moediger dan bij iemands mogelijkheden past in positieve maar ook in negatieve zin'', aldus het WikiWoordenboek."
       ,(citeer :titel "Vermetel — WikiWoordenboek" :auteur "WikiWoordenboek" :datum "2018" :url "https://nl.wiktionary.org/w/index.php?title=vermetel&oldid=3749075" :geraadpleegd "2019-06-19"))
   '(p "In het bijzonder is deze webstek vernoemd naar de betekenis van ``vermetel'' zoals gebruikt in de intuïtionistische wiskunde, een tak van de wiskunde waartoe de auteur behoort. Het ntuïtionistisme verwerpt een aantal aspecten van de klassieke logica, het meest in het oog springende daarvan zijnde de wet van de uitgesloten derde. Voor een intuïtionist moet een bewijs constructief zijn, een algoritme geven dat het probleem oplost. De intuïtionist is niet tevreden met zeggen dat het onmogelijk is dat het probleem onoplosbaar is. Mocht een klassieke wiskundige beweren dat die de oplossing van het probleem weet, een bewijs kent, het bestaan van iets te hebben aangetoond, zonder de constructie te hebben, dan is die in intuïtionistische opvatting dus overmoedig, omdat de wiskundige dan zegt dat iets bestaat zonder een (aanvaardbare) reden te kunnen aanwijzen.")
   '(p "Nu is het mogelijk om de vermetele uitspraken van de klassieke wiskundige toch een zinnige intuïtionistische betekenis toe te wijzen waarin de beweringen wel degelijk waar zijn, en in deze interpretatie impliceren de intuïtionistische noties van bestaan de zwakkere van klassieke wiskundigen. Hierin zien we een sterkte van het intuïtionisme — omdat het een fijnere notie heeft van proposities, kan het preciezer zijn over kwesties van constructies.")
   '(p "Het andere voordeel van het intuïtionisme is dat intuïtionistische bewijzen informatie dragen. Veel klassieke wiskundigen nemen, zonder het te weten, aan dat het bestaan van een object equivalent is aan het aantonen van de onmogelijkheid van het ontbreken, en tegelijkertijd dat het equivalent is aan het aangewezen hebben van dit object. De distinctie tussen de laatste twee is voor de klassieke wiskundige meestal onbenullig, maar heeft soms een nare angel. Beschouw namelijk de categorietheoreet die wil aantonen dat een bepaalde constructie natuurlijk is, en hierbij aanneemt dat er een eenduidige manier is om limieten te nemen in elke volledige categorie. Dit is niet vanzelf het geval voor de klassieke wiskundige; er moeten keuzeaxioma's aan te pas komen om van een familie ``bestaande'' objecten ook een vertegenwoordiger van elke klassen vast te leggen. Een intuïtionist weet dat de juiste notie van limiet precies bestaat uit het aanwijzen van elk zo'n vertegenwoordiger, dus bij constructie heeft men dit probleem niet. De klassieke wiskundige gaat hier normaliter aan voorbij, zonder door te hebben dat die in feite de limieten intuïtionistisch behandelt.")
   '(p "In elk geval gebruikt de auteur deze naam slechts halfironisch, omdat deze webstek nou eenmaal niet terugdeinst voor een gepeperde mening.")
   '(h1 "de webstek")
   `(p "Deze webstek draait op vrije software, vrijgegeven onder de licentie " (a :href ,(url-naar 'statisch-bestand '("agpl-3.0.txt")) :rel "license" "Affero GPL 3.0") ". De makkelijkste manier om bij de broncode te komen is " (a :href "https://gitlab.com/vermetel/vermetel.info/" "op GitLab") ". " ,(link-contact "Laat het vooral weten") " als GitLab voor jou problematisch is, dan regelen we een ander distributieformaat.")
   '(p "Op dit moment gebruiken we geen JavaScript. Zodra we dat wel doen, zal hier een tabelletje verschijnen met informatie over licenties.")
   '(h1 "de auteur")
   '(p "De auteur van deze webstek is een intuïtionistisch wiskundige en/of programmeur en gebruikt deze uithoek van het wereld wijde web om, naast leuke speeltjes, hun ervaringen en meningen te verspreiden. Afhankelijk van jouw persoonlijke notie van constructie kunnen deze roepingen samenvallen of slechts verwant zijn. Voornaamwoord: hen/hun (of elk aannemelijk bod). Hoewel het niet te lastig zou moeten zijn om achter overige persoonsgegevens te komen, een verzoek om de persoonsvrede te respecteren en niet te spelen met mijn privé. En los daarvan, zou het niet mooier zijn om zonder aanzien des persoons mij aan te tijgen de lusten mijner moeders?")
   '(p :id "contactgegevens" "Mocht je zin hebben om mij aan te tijgen de lusten mijner moeders (of om welke andere reden dan ook contact opnemen), dan kun je me het beste bereiken door een e-mail te versturen aan " (a :href "mailto:vermetel@vermetel.info" "het voor de hand liggende adres") "."))

  :menuutje (bouw-menuutje
             :laatst-bijgewerkt "2019-06-25"
             :citaat "We're each of us alone, to be sure. What can you do but hold your hand out in the dark?"))
